const mediaQuery = window.matchMedia('(min-width: 1440px)');

mediaQuery.addEventListener('change', (event) => {
	const visibleArticle = document.querySelector('article:not([style="display: none;"])');
	if (event.matches) {
		visibleArticle.style.display = 'flex';
	} else {
		visibleArticle.style.display = 'block';
	}
});

const leftButtons = document.querySelectorAll('.left-button');
const rightButtons = document.querySelectorAll('.right-button');
const articles = document.querySelectorAll('article');

let index = 0;

function showArticle() {
	articles.forEach((article) => {
		if (articles[index] === article) {
			article.style.display = mediaQuery.matches ? 'flex' : 'block';
		} else {
			article.style.display = 'none';
		}
	});
}

function showPrevArticle() {
	if (--index === -1) {
		index = articles.length - 1;
	}
	showArticle();
}

function showNextArticle() {
	if (++index === articles.length) {
		index = 0;
	}
	showArticle();
}

leftButtons.forEach((leftButton) => {
	leftButton.addEventListener('click', showPrevArticle);
});

rightButtons.forEach((rightButton) => {
	rightButton.addEventListener('click', showNextArticle);
});

showArticle();
